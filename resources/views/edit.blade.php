@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Enter new data</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/edit') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Name</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name"
                                           value="{{$user->name}}" required>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('surname') ? ' has-error' : '' }}">
                                <label for="surname" class="col-md-4 control-label">Surname</label>

                                <div class="col-md-6">
                                    <input id="surname" type="text" class="form-control" name="surname"
                                           value="{{$user->surname}}" required>

                                    @if ($errors->has('surname'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('surname') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('born_date') ? ' has-error' : '' }}">
                                <label for="born_date" class="col-md-4 control-label">Born date</label>

                                <div class='col-md-6'>
                                    <input id="born_date" data-position="right top" class="form-control" name="born_date" placeholder="01.01.2020"
                                           value="{{$user->born_date}}" required>

                                    @if ($errors->has('born_date'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('born_date') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('contact') ? ' has-error' : '' }}">
                                <label for="contact" class="col-md-4 control-label">Contact</label>

                                <div class="col-md-6">
                                    <input id="contact" type="text" class="form-control"
                                           name="contact" placeholder="(+380)0000001" value="{{$user->contact}}"
                                           required>
                                </div>
                                @if ($errors->has('contact'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('contact') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                                <label for="contact" class="col-md-4 control-label">
                                    Your location</label>

                                <div class="col-md-6">
                                    <input id="location" type="text" class="form-control"
                                           name="location" value="{{$user->location}}" required>
                                </div>
                                @if ($errors->has('location'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('location') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('more_info') ? ' has-error' : '' }}">
                                <label for="more_info" class="col-md-4 control-label">About me</label>

                                <div class="col-md-6">
                                    <input id="more_info" type="text" class="form-control"
                                           name="more_info" value="{{$user->more_info}}">
                                </div>
                                @if ($errors->has('more_info'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('more_info') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn fa-user"></i> Edit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
