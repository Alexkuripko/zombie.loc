<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css')}}" rel="stylesheet">

</head>
<body>

<div id="app">
    <div class="header">
    </div>
    <div id="wrapper">

        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">

                </li>
                <li>
                    @if(!Auth::guest())
                        <a href="{{ url('/profile') }}/{{ Auth::user()->id }}">Profile</a>
                    @endif
                </li>
                <li>
                    <a href="#">Messages</a>
                </li>
                <li>
                    <a href="{{action('UsersController@show', 'name')}}">Users</a>
                </li>
                <li>
                    <a href="#">About</a>
                </li>
                <li>
                    @if (Auth::guest())
                        <a href="{{ route('login') }}">Login</a>
                    @else
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                            Logout
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                              style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    @endif
                </li>
            </ul>
        </div>

        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <i class="fas fa-arrow-circle-left" id="menu-toggle"></i>
                    </div>
                </div>
            </div>
        </div>
        <div id="content">
            @yield('content')
        </div>
{{--        <div class="footer">--}}
{{--            <hr>--}}
{{--            <p class="footer-text">Created by Alexandr Kuripko</p>--}}
{{--        </div>--}}
    </div>
</div>
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>

</body>
</html>
