@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        My profile
                        @if (Auth::user()->id == $user->id)
                            <a class="text-info btn-edit" href="{{ url('/edit') }}">Edit profile</a>
                        @endif
                    </div>
                    <div class="panel-body">
                        <img id="avatar-img" src="{{asset('images/'.$user->avatar)}}" alt="avatar" data-toggle="modal"
                             data-target="#loadPhotoModal">
                        <p class="profile-user_name">{{$user->name}} {{$user->surname}}</p>
                        <div class="contact-info">
                            <p>Contacts:</p>
                            <ul>
                                <li>{{$user->email}}</li>
                                <li>{{$user->contact}}</li>
                            </ul>
                        </div>
                        @if(Auth::user()->id != $user->id)
                            @if($user->eaten)
                                <button id="eaten" class="btn btn-danger user-status">Eaten</button>
                            @else
                                <button id="alive" class="btn btn-success user-status">Alive</button>
                            @endif
                            <button class="btn btn-primary write-message">Write message</button>
                        @endif
                        <hr id="profile-hr">
                        <div class="user-info">
                            {{$user->more_info}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="loadPhotoModal" tabindex="-1" role="dialog" aria-labelledby="loadPhotoModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="loadPhotoModalLabel">
                        Select photo to upload</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="/updatePhoto">
                    <div class="modal-body">
                        <input type="file" name="photo">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary create" name="submit">Upload</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

