@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="panel-title">User list</span>

                        <div class="dropdown">
                            <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                Sort by
                                <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="{{action('UsersController@show', 'name')}}">Name</a></li>
                                <li><a href="{{action('UsersController@show', 'born_date')}}">Age</a></li>
                                <li><a href="{{action('UsersController@show', 'date_of_last_active')}}">Last active</a>
                                </li>
                                <li><a href="{{action('UsersController@show', 'eaten')}}">Eaten</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel-body">
                        @foreach($users as $user)
                            <hr id="hr-line">
                            <div class="user-block">
                                <a href="{{ url('/profile') }}/{{$user->id}}" class="user-list_link">
                                    <img class="user-avatar" src="{{asset('images/'.$user->avatar)}}" width="100px">
                                    <p class="user-name">{{$user->name}} {{$user->surname}}</p>
                                    <p class="user-email">{{$user->email}}</p>
                                </a>
                                <button type="button" class="btn btn-primary write-message_btn">Write message</button>
                            </div>
                        @endforeach
                    </div>
                    {{$users->render()}}
                </div>
            </div>
        </div>
    </div>

@endsection