$(document).ready(function () {
    $("#menu-toggle").click(function (e) {
        if ($(this).hasClass('fa-arrow-circle-left')) {
            $(this).removeClass('fa-arrow-circle-left');
            $(this).addClass('fa-arrow-circle-right');
        } else {
            $(this).removeClass('fa-arrow-circle-right');
            $(this).addClass('fa-arrow-circle-left');
        }
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");

    });
});
