<?php


Auth::routes();

Route::get('/', 'ProfileController@index')->middleware('auth');

Route::get('/profile/{id?}', 'ProfileController@index')->middleware('auth');

Route::get('/users/{sort?}', 'UsersController@show');

Route::post('/updatePhoto', 'ProfileController@updatePhoto')->middleware('auth');

Route::match(['get', 'post'],'/edit', 'ProfileController@edit')->middleware('auth');