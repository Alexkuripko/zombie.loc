<?php

namespace App\Http\Controllers;

use App\Users;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function show(Request $request)
    {

        $sort = $request->sort;

        $userList = Users::where('id', '>', 1)->orderBy($sort, 'asc')->paginate(10);

        return view('users', [
            'users' => $userList
        ]);
    }
}
