<?php

namespace App\Http\Controllers;

use App\Users;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;


class ProfileController extends Controller
{


    public function index(Request $request, $userId = null)
    {

        if ($userId == null) $userId = Auth::id();

        $user = Users::where('id', $userId)->get();

        return view('profile', [
            'user' => $user[0]
        ]);
    }

    public function edit(Request $request)
    {

        $userId = Auth::user()->id;

        $user = Users::where('id', $userId)->get();

        if ($request->isMethod('post')) {

            $this->validate($request, ['name' => 'required|min:3|max:20',
                'surname' => 'required|min:3|max:20',
                'born_date' => 'date|required',
                'contact' => 'required|min:12|max:13',
                'location' => 'required|min:1|max:11',
                'more_info' => 'max:255',]);

            /** @var object $request */
            $request = $request->all();
            unset($request['_token']);
            Users::where('id', $userId)
                ->update($request);

            return redirect('/profile');
        }

        return view('edit', [
            'user' => $user[0]
        ]);
    }

    public function updatePhoto(Request $request)
    {
        $photo = $request->file('photo');

        dd($photo);
    }
}
