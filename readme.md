
   Project deployment instruction:
 
      1. clone git from https://Alexkuripko@bitbucket.org/Alexkuripko/zombie.loc.git
      2. composer install
      3. npm install
      4. configure database connections in .env
      5. php artisan migrate